(module.exports = config = require('../config/config.json')),
  (express = require('express')),
  (bodyParser = require('body-parser')),
  // (httpStatus = require('http-status')),
  (morgan = require('morgan')),
  (app = express());

//Middlewares
app.use(morgan('dev'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(express.json({ limit: '550mb' }));

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.setHeader('Access-Control-Allow-Methods', 'POST, GET, DELETE, OPTIONS');
  next();
});

app.listen(config.port, () => {
  console.log(`escuchando ${config.port}`);
}).timeout = 360000;

//Global import bEnterbyte and add routes path
require('./config-app.js');

app.use(function (err, req, res, next) {
  console.error(err);
  res.status(500).send(null);
});
