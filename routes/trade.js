const router = express.Router();

router.get('/get-assets', (req, res) => {
  bTrade.Trade.getAssets()
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

router.get('/get-assets-pair', (req, res) => {
  bTrade.Trade.getAssetPairs()
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

router.get('/get-ticker-info', (req, res) => {
  const { pair } = req.query;
  bTrade.Trade.getTickerInfo(pair)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

router.get('/get-order-book', (req, res) => {
  const { pair } = req.query;
  bTrade.Trade.getOrderBook(pair)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

router.get('/get-recent-trades', (req, res) => {
  const { pair } = req.query;
  bTrade.Trade.getRecentTrades(pair)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

router.get('/get-recent-spread-data', (req, res) => {
  const { pair } = req.query;
  bTrade.Trade.getRecentSpreadData(pair)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

router.get('/add-standar-order', (req, res) => {
  const { pair } = req.query;
  bTrade.Trade.addStandardOrder(pair)
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      res.send(err);
    });
});

module.exports = router;
