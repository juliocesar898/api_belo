
const router = express.Router();

router.get('/health-check', (req, res, next) => {
  res.send('everything is Ok');
});

module.exports = router;