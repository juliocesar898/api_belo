const axios = require('axios');

module.exports = {
  getAssets: () => {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api.kraken.com/0/public/Assets')
        .then((result) => {
          resolve(result.data.result);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getAssetPairs: () => {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api.kraken.com/0/public/AssetPairs')
        .then((result) => {
          resolve(result.data.result);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getTickerInfo: (pair) => {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api.kraken.com/0/public/Ticker', { params: { pair } })
        .then((result) => {
          resolve(result.data.result);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getOrderBook: (pair) => {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api.kraken.com/0/public/Depth', { params: { pair } })
        .then((result) => {
          resolve(result.data.result);
        })
        .catch((err) => {
          reject();
        });
    });
  },
  getRecentTrades: (pair) => {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api.kraken.com/0/public/Trades', { params: { pair } })
        .then((result) => {
          resolve(result.data.result);
        })
        .catch((err) => {
          reject();
        });
    });
  },
  getRecentSpreadData: (pair) => {
    return new Promise((resolve, reject) => {
      axios
        .get('https://api.kraken.com/0/public/Spread', { params: { pair } })
        .then((result) => {
          resolve(result.data.result);
        })
        .catch((err) => {
          reject();
        });
    });
  },
};
